<?php

namespace Database\Seeders;
use App\Models\posteos;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class posteosSeeder extends Seeder
{
    public function run(): void
    {
        $posteos = [
            [
                'fecha' => now(),
                'contenido' => 'En esta ocasion les taremos las mejores caidas del 2012',
                'adjunto_url' => '<iframe width="640" height="480" src="https://www.youtube.com/embed/zdJWWmZZX6s" title="Caidas y ostias graciosas 2012" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>',
                'usuario_id' => 1,
                'hora'=> now()
            ],
            [
                'fecha' => now(),
                'contenido' => 'Troleos epicos 2015',
                'adjunto_url' => '<iframe width="853" height="480" src="https://www.youtube.com/embed/FxvEGBzWZT8" title="Trolleos Epicos - Videos Random Xolo Fail" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>',
                'usuario_id' => 3,
                'hora'=> now()
            ],
            [
                'fecha' => now(),
                'contenido' => 'Olimpia el más grande',
                'adjunto_url' => 'https://clubolimpia.com.py/wp-content/uploads/2023/08/Olimpia@2x-1.png',
                'usuario_id' => 2,
                'hora'=> now()
            ],
            [
                'fecha' => now(),
                'contenido' => 'Wasa',
                'adjunto_url' => '',
                'usuario_id' => 4,
                'hora'=> now()
            ],
            [
                'fecha' => now(),
                'contenido' => 'Hakurooooo',
                'adjunto_url' => '',
                'usuario_id' => 5,
                'hora'=> now()
            ],
            
        ];

        foreach ($posteos as $posteosData) {
            posteos::create($posteosData);
        }
    }
}