<?php

namespace Database\Seeders;
use App\Models\redes_sociales;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class redes_socialesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $redes_sociales = [
            [
                'nombre' => 'twitter',
                'url' => 'https://twitter.com/?lang=es',
                'logo_url' => 'https://www.eltiempo.com/files/article_main_1200/uploads/2023/07/24/64be65d19b492.jpeg',
                'activo' => true
            ],
            [
                'nombre' => 'facebook',
                'url' => 'https://www.facebook.com/',
                'logo_url' => 'https://cdn-icons-png.flaticon.com/512/4701/4701482.png',
                'activo' => true
            ],
            [
                'nombre' => 'Instagram',
                'url' => 'https://www.instagram.com/',
                'logo_url' => 'https://acortar.link/qJVGUO',
                'activo' => true
            ],
            [
                'nombre' => 'TikTok',
                'url' => 'https://www.tiktok.com/es/',
                'logo_url' => 'https://acortar.link/Oc7ZbC',
                'activo' => true
            ],
            [
                'nombre' => 'WhatsApp',
                'url' => 'https://web.whatsapp.com/',
                'logo_url' => 'https://cdn-icons-png.flaticon.com/512/1384/1384095.png',
                'activo' => true
            ],
        ];

        foreach ($redes_sociales as $redes_socialesData) {
            redes_sociales::create($redes_socialesData);
        }
    }
}
