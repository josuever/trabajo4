<?php

namespace Database\Seeders;
use App\Models\respuestas;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class respuestasSeeder extends Seeder
{
    public function run(): void
    {
        $respuestas = [
            [
                'usuario_id' => 3,
                'posteo_id' => 1,
                'fecha'=> now(),
                'mensaje' => 'Nde ay legalmente kp'
            ],
            [
                'usuario_id' => 1,
                'posteo_id' => 1,
                'fecha'=> now(),
                'mensaje' => 'Naaaa, buenisimo el video xD'
            ],
            
            [
                'usuario_id' => 4,
                'posteo_id' => 2,
                'fecha'=> now(),
                'mensaje' => 're viejardo xd, pero wenisimo'
            ],
            [
                'usuario_id' => 1,
                'posteo_id' => 5,
                'fecha'=> now(),
                'mensaje' => 'si brother'
            ],
            [
                'usuario_id' => 5,
                'posteo_id' => 1,
                'fecha'=> now(),
                'mensaje' => 'locuraaaa'
            ],
            
        ];

        foreach ($respuestas as $respuestasData) {
            respuestas::create($respuestasData);
        }
    }
}
