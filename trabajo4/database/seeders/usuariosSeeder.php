<?php

namespace Database\Seeders;
use App\Models\usuarios;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class usuariosSeeder extends Seeder
{
    public function run(): void
    {
        $usuarios = [
            [
                'red_social_id' => 3,
                'usuario' => 'xx_josuever',
                'contrasenha' => 'paquequeresaber',
                'activo' => true,
                'handle'=> 'josu'
            ],
            [
                'red_social_id' => 3,
                'usuario' => 'adrian_aquino',
                'contrasenha' => 'boquitaelmasgrande',
                'activo' => true,
                'handle'=> 'el_gfe'
            ],
            [
                'red_social_id' => 1,
                'usuario' => 'alfredo_coronel',
                'contrasenha' => 'blackops',
                'activo' => true,
                'handle'=> 'el_mas_capo'
            ],
            [
                'red_social_id' => 2,
                'usuario' => 'raul_samaniego',
                'contrasenha' => 'seamericana',
                'activo' => true,
                'handle'=> 'el_sama'
            ],
            [
                'red_social_id' => 4,
                'usuario' => 'rodrigo_ortiz',
                'contrasenha' => 'tiktokrodrigo',
                'activo' => true,
                'handle'=> 'si'
            ],
        ];

        foreach ($usuarios as $usuariosData) {
            usuarios::create($usuariosData);
        }
    }
}
