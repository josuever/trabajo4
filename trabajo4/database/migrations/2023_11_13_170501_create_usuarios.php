<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            
            //Clave foranea de redes_sociales
            $table->unsignedBigInteger('red_social_id')->comment('clave foranea de redes_sociales');
            $table->foreign('red_social_id')->references('id')->on('redes_sociales');
            
            $table->string('usuario', 60)->comment('Nombre del usuario');
            $table->string('contrasenha', 60)->comment('Contraseña del usuario');
            $table->boolean('activo')->default(true)->comment('activo');   
            $table->string('handle', 20)->comment('Identificador de Usuario');         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('usuarios');
    }
};
