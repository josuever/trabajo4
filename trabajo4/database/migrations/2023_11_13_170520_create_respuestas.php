<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('respuestas', function (Blueprint $table) {
            $table->id();

            //Clave foranea de usuarios
            $table->unsignedBigInteger('usuario_id')->comment('Clave foranea de usuarios');
            $table->foreign('usuario_id')->references('id')->on('usuarios');

            //Clave foranea de posteos
            $table->unsignedBigInteger('posteo_id')->comment('Clave foranea de posteos');
            $table->foreign('posteo_id')->references('id')->on('posteos');

            $table->date('fecha')->comment('Fecha del comentario al posteo');
            $table->text('mensaje')->comment('Comentario del posteo');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('respuestas');
    }
};
