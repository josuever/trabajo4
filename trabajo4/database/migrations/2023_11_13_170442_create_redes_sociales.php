<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('redes_sociales', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 40)->comment('El nombre de la red_social');
            $table->string('url', 200)->nullable()->comment('Url de la red_social');
            $table->string('logo_url', 200)->comment('Url del logo');
            $table->boolean('activo')->default(true)->comment('Campo para borrado logico');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('redes_sociales');
    }
};
