<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('posteos', function (Blueprint $table) {
            $table->id();
            $table->date('fecha')->comment('Fecha del posteo del usuario');
            $table->text('contenido')->comment('Contenido del posteo');
            $table->string('adjunto_url', 300)->nullable()->comment('Url del posteo');

            //Clave foranea de usuarios
            $table->unsignedBigInteger('usuario_id')->comment('Clave foranea de usuarios');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
           
            $table->time('hora')->comment('Hora del posteo del usuario');
            $table->timestamps(); 
        });

    }
    
    public function down(): void
    {
        Schema::dropIfExists('posteos');
    }
};
