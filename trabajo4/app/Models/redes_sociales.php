<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Redes_sociales extends Model
{
    use HasFactory;
    protected $table = 'redes_sociales';
    protected $fillable = ['nombre', 'url', 'logo_url', 'activo'];

    public function Usuarios()
    {
        return $this->hasMany(Usuarios::class, 'red_social_id', 'id');
    }
}
