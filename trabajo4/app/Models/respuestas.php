<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Respuestas extends Model
{
    use HasFactory;

    protected $table = 'respuestas';

    protected $fillable = ['usuario_id', 'posteo_id', 'fecha', 'mensaje'];

    public function Usuarios()
    {
        return $this->belongsTo(Usuarios::class, 'usuario_id', 'id');
    }

    public function Posteos()
    {
        return $this->belongsTo(Posteos::class, 'posteo_id', 'id');
    }
}
