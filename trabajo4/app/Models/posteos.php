<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posteos extends Model
{
    use HasFactory;

    protected $table = 'posteos';

    protected $fillable = ['fecha', 'contenido', 'adjunto_url', 'usuario_id', 'hora'];

    public function Usuarios()
    {
        return $this->belongsTo(Usuarios::class, 'usuario_id', 'id');
    }
    public function Respuestas()
    {
        return $this->hasMany(Respuestas::class, 'posteo_id', 'id');
    }
}
