<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Usuarios extends Model
{
    use HasFactory;
    protected $table = 'usuarios';
    protected $fillable = ['red_social_id', 'usuario', 'contrasenha', 'activo', 'handle'];

    public function Redes_sociales()
    {
        //Se agrega una relacion a otro modelo clave forarea, primary key de la otra tabla
        return $this->belongsTo(Redes_sociales::class, 'red_social_id', 'id');
    }
    public function Posteos()
    {
        return $this->hasMany(Posteos::class, 'usuario_id', 'id');
    }
    public function Respuestas(): HasMany
    {
        return $this->hasMany(Respuestas::class, 'usuario_id', 'id');
        //->where('activo', true);
    }

    
}
