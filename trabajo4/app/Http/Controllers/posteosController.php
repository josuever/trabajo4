<?php

namespace App\Http\Controllers;
use App\Models\Posteos;
use App\Models\Usuarios;
use Illuminate\Http\Request;

class PosteosController extends Controller
{
    public function index(string $id)
    {
      $usuarios = Usuarios::with('Respuestas')->find($id);
        $posteos = $usuarios->Respuestas;
        return view('posteos.ver', [
            'usuario' => $usuarios,
            'posteos' => $posteos
          ]);
      
    }    
     
    public function indexIndex()
    {
      $posteos = Posteos::with('usuarios')->orderBy('fecha', 'desc')->orderByDesc('hora')->get();
    //   dd($posteos);
      return view('index',  ['posteos' => $posteos]);
    }
    
    public function obtenerPosteo($id)
{
    // Obtener el posteo con el ID proporcionado y cargar la relación con el usuario y respuestas
    $posteos = Posteos::with(['usuarios', 'respuestas.usuarios'])->find($id);

    if (!$posteos) {
        return response()->json(['error' => 'Posteo no encontrado'], 404);
    }

    $datos = [
        'id' => $posteos->id,
        'contenido' => $posteos->contenido,
        'adjunto_url' => $posteos->adjunto_url,
        'usuario' => [
            'id' => $posteos->usuarios->id,
            'nombre' => $posteos->usuarios->usuario,
        ],
        'respuestas' => $posteos->respuestas->map(function ($respuesta) {
            return [
                'id' => $respuesta->id,
                'mensaje' => $respuesta->mensaje,
                'usuario' => [
                    'id' => $respuesta->usuarios->id,
                    'nombre' => $respuesta->usuarios->usuario,
                ],
            ];
        }),
    ];

    return view('posteos.ver', ['posteos' => $posteos, 'datos' => $datos]);
}

    
    
    public function indexPosteos()
    {
      $posteos = Posteos::with('usuarios')->orderBy('fecha', 'desc')->orderByDesc('hora')->get();
      
      return view('posteos.listar',  ['posteos' => $posteos]);
    }

    
    public function store(Request $request)
    {
        // contenido, adjunto_url, usuario_id, 
        $posteos = new Posteos();
        $posteos->contenido = $request->contenido;
        $posteos->usuario = '1';
        $posteos->fecha = date("d-m-Y h:i:s");
        $posteos->adjunto_url = $request->adjunto_url;
        $posteos->save();

        return redirect()->route('valoraciones-crear', ['id' => $request->producto_id]);
    }
}

