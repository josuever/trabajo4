<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PosteosController;
use App\Http\Controllers\Redes_socialesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// })-> name('index');;    

Route::get('/', [PosteosController::class, 'indexIndex'])
    ->name('index');

//Posteos
Route::get('/posteos', [PosteosController::class, 'indexPosteos'])
    ->name('posteos_listar');

Route::get('/posteos/ver/{id}', [PosteosController::class, 'obtenerPosteo'])
    ->name('posteos_ver');
    
//Redes sociales
Route::get('/redes_sociales', [Redes_socialesController::class, 'index'])
    ->name('redes_sociales_listar');
