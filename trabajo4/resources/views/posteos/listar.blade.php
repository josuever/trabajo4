<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Posteos</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('bootstrap.min.css') }}">

    <style>
        /* Custom styles */
        body {
            font-family: 'Arial', sans-serif;
        }

        .btn-success {
            background-color: #28a745; /* Green color for success buttons */
            color: #fff;
        }

        .btn-danger {
            background-color: #dc3545; /* Red color for danger buttons */
            color: #fff;
        }

        .table th,
        .table td {
            text-align: center;
        }

        .btn-volver {
            background-color: #007bff; /* Blue color for volver button */
            color: #fff;
        }

        .btn-volver:hover {
            background-color: #0056b3; /* Darker blue on hover */
        }
    </style>
</head>

<body class="bg-light">

    <div class="container mt-4">
        <div class="row">
            <div class="col-12">
                <h1 class="display-4 text-center font-weight-bold">Listado de Posteos</h1>
            </div>
        </div>

        @if (count($posteos) == 0)
            {{-- No hay posteos --}}
            <div class="row mt-4">
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        No existen Posteos.
                    </div>
                </div>
            </div>
        @else
            {{-- Mostrar Tabla --}}
            <div class="row mt-4">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Contenido</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Hora</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posteos as $posteo)
                                    <tr>
                                        <th>{{ $posteo->id }}</th>
                                        <td>{{ $posteo->usuarios->usuario }}</td>
                                        <td>{{ $posteo->contenido }}</td>
                                        <td>{{ $posteo->fecha }}</td>
                                        <td>{{ $posteo->hora }}</td>
                                        <td>
                                            <a class="btn btn-sm btn-success"
                                                href="{{ route('posteos_ver', $posteo->id) }}">
                                                Ir al Post
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif

        <div class="row mt-4">
            <div class="col-12 text-center">
                <a class="btn btn-volver" href="{{ route('index') }}">Volver al Inicio</a>
            </div>
        </div>
    </div>

    <!-- Bootstrap JS (optional) -->
    <script src="{{ asset('bootstrap.min.js') }}"></script>
</body>

</html>
