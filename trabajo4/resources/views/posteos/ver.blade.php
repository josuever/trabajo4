<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ver Post</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <style>
        /* Custom styles */
        body {
            font-family: 'Arial', sans-serif;
        }

        .btn-success {
            background-color: #28a745; /* Green color for success buttons */
            color: #fff;
        }

        .btn-danger {
            background-color: #dc3545; /* Red color for danger buttons */
            color: #fff;
        }

        .post-section {
            margin-top: 20px;
        }

        .response {
            margin-top: 10px;
            border-left: 3px solid #007bff; /* Blue color for response border */
            padding-left: 10px;
            font-size: 16px;
        }

        .jumbotron {
            background-color: #f8f9fa; /* Light grey background for the jumbotron */
            padding: 20px;
        }

        h1.display-4, p.card-text {
            font-size: 24px;
            font-weight: bold;
        }

        .card.post-section {
            border: 1px solid #dee2e6; /* Light grey border for the card */
            border-radius: 10px;
        }

        .card-body {
            font-size: 18px;
        }

        img.img-fluid {
            max-width: 100%;
            height: auto;
            border-radius: 10px;
        }
    </style>
</head>

<body>
    <div class="container mt-5">
        <div class="jumbotron">
            <h1 class="display-4">Publicado por:</h1>
            <p class="card-text">{{ $posteos['usuarios']['usuario'] }}</p>
        </div>

        <div class="card post-section">
            <div class="card-body">
                @if ($posteos['adjunto_url'])
                    @if (Str::endsWith(strtolower($posteos['adjunto_url']), ['.jpg', '.jpeg', '.png', '.gif']))
                        <img src="{{ $posteos['adjunto_url'] }}" class="img-fluid mb-3" alt="Imagen Adjunta">
                    @elseif (Str::endsWith(strtolower($posteos['adjunto_url']), ['.mp4', '.avi', '.mov']))
                        <div class="embed-responsive embed-responsive-16by9 mb-3">
                            {{ $posteos['adjunto_url'] }}
                        </div>
                    @elseif (Str::contains($posteos['adjunto_url'], 'youtube.com'))
                        <div class="embed-responsive embed-responsive-16by9 mb-3">
                            {!! $posteos ['adjunto_url']!!}
                        </div>
                    @else
                        <p class="card-text">Adjunto no reconocido</p>
                    @endif
                @endif
            </div>

            <div class="card post-section">
                <div class="card-body">
                    <p class="d-inline"><strong>{{ $posteos['usuarios']['usuario'] }}</strong>: {{ $posteos['contenido'] }}</p>
                </div>
            </div>

            <!-- Respuestas -->
            @if(count($datos['respuestas']) > 0)
                <div class="mt-4 post-section">
                    <h2>Respuestas:</h2>
                    <ul class="list-group">
                        @foreach($datos['respuestas'] as $respuesta)
                            <li class="list-group-item response">
                                <strong>{{ $respuesta['usuario']['nombre'] }}:</strong> {{ $respuesta['mensaje'] }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @else
                <p class="mt-4">No hay respuestas para este posteo.</p>
            @endif
            <!-- Fin de Respuestas -->

            <a href="{{ route('posteos_listar') }}" class="btn btn-success mt-3">Volver</a>
        </div>
    </div>

    <script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>
