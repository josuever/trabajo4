<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>

    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
</head>
<body class="bg-light">

    <div class="container mt-5">
        <div class="jumbotron">
            <h1 class="display-4">Proyecto: Redes Sociales</h1>
            <p class="lead">Alumno: Josué Vera</p>
        </div>
    
        <div class="row">
            <div class="col-md-6">
                <p class="lead">DER</p>
                <img src="{{asset('DER.png')}}" alt="DER" class="img-fluid rounded">
            </div>
    
            <div class="col-md-6">
                <div class="container">
                    <h2 class="subtitle">Redes Sociales</h2>
                    <div class="panel">
                        <a class="btn btn-link" href="{{ route('redes_sociales_listar') }}">
                            Listado de Redes Sociales
                        </a>
                        <br>
                        <a class="btn btn-link" href="{{ route('posteos_listar') }}">
                            Listado de Posteos
                        </a>
                        <br>
                    
                        <a class="btn btn-link" href="{{ route('posteos_ver', $posteos[0]->id) }}">
                            Ver primer post
                        </a>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    

</body>
</html>
